class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery  #with: :exception

  protected
  # Возвращает аутентифицированного насайте пользователя по его id или nil,
  # если он еще не вошел на сайт.
  def current_user
    return unless session[:user_id]
    @current_user ||= User.find_by_id(session[:user_id])
  end

  # Делает хелпер current_user доступным во всех шаблонах приложения.
  helper_method :current_user

  # Фильтровочный метод для принуждения ко входу на сайт.
  # Применяется в колбеке before_action любого контроллера, который нужно защищать.
  def authenticate
    logged_in? || access_denied
  end

  # Проверяет вошел ли указанный пользователь на сайт.
  def logged_in?
    current_user.is_a? User
  end

  # Делает хелпер logged_in? доступным во всех шаблонах приложения.
  helper_method :logged_in?

  def access_denied
    redirect_to login_path, notice: 'Please log in to continue' and return false
  end
end
